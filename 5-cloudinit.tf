# refer to https://grantorchard.com/dynamic-cloudinit-content-with-terraform-file-templates/

# for user account setting
data "template_file" "user_data" {
  count = length(var.osp_names) + length(var.ceph_names) + 1
  template = file("${path.module}/templates/cloud_init.cfg")
}

# for Hostname setting
data "template_file" "osp_metadata" {
  count = length(var.osp_names)
  template = file("${path.module}/templates/metadata.yaml")
  vars = {
    hostname = var.osp_names[count.index]
  }
}

# for Hostname setting
data "template_file" "ceph_metadata" {
  count = length(var.ceph_names)
  template = file("${path.module}/templates/metadata.yaml")
  vars = {
    hostname = var.ceph_names[count.index]
  }
}

# for Hostname setting
data "template_file" "deploy_metadata" {
  template = file("${path.module}/templates/metadata.yaml")
  vars = {
    hostname = var.deploy_name
  }
}

# for Network setting
data "template_file" "osp_network_config" {
  count = length(var.osp_names)
  template = file("${path.module}/templates/osp_network_config.cfg")

  vars = {
    deploy_addr = "${var.deploy_addr}"
    monitor_addr = "${var.monitor_addr}"
    storage_addr = "${var.storage_addr}"
    internal_addr = "${var.internal_addr}"
    external_addr = "${var.external_addr}"
    ip_num  = "${var.osp_ipnum}${count.index}"
    gateway = "192.168.0.1"
  }
}

# for Ceph Network setting
data "template_file" "ceph_network_config" {
  count = length(var.ceph_names)
  template = file("${path.module}/templates/ceph_network_config.cfg")

  vars = {
    deploy_addr = "${var.deploy_addr}"
    monitor_addr = "${var.monitor_addr}"
    storage_addr = "${var.storage_addr}"
    external_addr = "${var.external_addr}"
    ip_num  = "${var.ceph_ipnum}${count.index}"
    gateway = "192.168.0.1"
  }
}

# for Deploy Network setting
data "template_file" "deploy_network_config" {
  template = file("${path.module}/templates/deploy_network_config.cfg")

  vars = {
    deploy_addr = "${var.deploy_addr}"
    storage_addr = "${var.storage_addr}"
    external_addr = "${var.external_addr}"
    ip_num  = "${var.deploy_ipnum}"
    gateway = "192.168.0.1"
  }
}
