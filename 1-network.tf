# It can not make, if you use the duplicated bridge name or network name or addresses.

resource "libvirt_network" "deploy" {
  name = "${var.prjname}-deploy"
  mode = "none"
  bridge = "virbr${var.osp_ipnum}0"
  addresses = ["${var.deploy_addr}.0/24"]
  autostart = true
}

resource "libvirt_network" "storage" {
  name = "${var.prjname}-storage"
  mode = "none"
  bridge = "virbr${var.osp_ipnum}1"
  addresses = ["${var.storage_addr}.0/24"]
  autostart = true
}

resource "libvirt_network" "monitor" {
  name = "${var.prjname}-monitor"
  mode = "none"
  bridge = "virbr${var.osp_ipnum}2"
  addresses = ["${var.monitor_addr}.0/24"]
  autostart = true
}

resource "libvirt_network" "internal" {
  name = "${var.prjname}-internal"
  mode = "none"
  bridge = "virbr${var.osp_ipnum}3"
  addresses = ["${var.internal_addr}.0/24"]
  autostart = true
}

resource "libvirt_network" "external" {
  name = "${var.prjname}-external"
  mode = "bridge"
  bridge = "br0"
  addresses = ["192.168.0.0/16"]
  autostart = true
}
